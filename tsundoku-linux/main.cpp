/*
 *** Includes
 */
#include <QtQuick>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2>
#include <QtQuickControls2/QQuickStyle>
#include <iostream>
#include <string>
#include <vector>
#include <filesystem>

/*
 *** Object Classes
 */
class Manga 
{
	public:
		std::string title;
		int chapters;
	Manga( std::string ti, int ch )
	{
		title = ti;
		chapters = ch;
	}
};

/*
 *** Static Classes
 */
class MajorUtils
{
	public:
		static int parseMangaDirectory(const std::string& directoryPath)
		{
			// parse here
			if ( std::filesystem::is_directory(directoryPath) ) { return 0; } else { return 1; };
		}
};

/*
 *** Main | Entry point 
 */
int main( int argc, char *argv[] )
{
	QGuiApplication app( argc, argv );
	
	// load icon font
	QFontDatabase fontDatabase;
	if ( fontDatabase.addApplicationFont("fonts/Ubuntu_Nerd_Font_Complete.ttf") == -1 )
	{
		std::cout << "Unable to load font" << std::endl;
	}
	
	// load QML gui
	QQmlApplicationEngine engine;
	engine.load( QUrl(QStringLiteral("main.qml")) );
	
	return app.exec();
}
