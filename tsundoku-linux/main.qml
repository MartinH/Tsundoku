import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 2.0
import QtQuick.Templates 2.0
import QtQuick.Controls.Universal

ApplicationWindow {
	title: "Tsudoku"
	width: 800
	height: 700
	visible: true
	
	Palette {
		id: palette
		light: "#FFFFFF" // use later
		dark: "#000000" // Panel Background
		text: "#d8d8d8" // Text Color
		base: "#161616" // Base Background
	}
	
	// Library title
	Rectangle {
		width: parent.width
		height: 40
		color: palette.dark
		x: 0
		y: 0
		z: 1
				
		Text {
			text: "My library"
			color: palette.text
			anchors.topMargin: 20
			horizontalAlignment: Text.AlignLeft
			verticalAlignment: Text.AlignVCenter
			anchors.left: parent.left
			anchors.verticalCenter: parent.verticalCenter
		}
	}
	
	// Seach icon button
	Rectangle {
		color: searchHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 240
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: searchHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Search button pressed", pressed)
		}
	}
	
	// Tag search icon button
	Rectangle {
		id: tagbutton
		color: tagHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 200
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: tagHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Tags button pressed", pressed)
		}
	}
	
	// Bookmarks icon button
	Rectangle {
		color: bookmarksHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 160
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: bookmarksHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Bookmarks button pressed", pressed)
		}
	}
	
	// Import icon button
	Rectangle {
		color: importHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 120
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: importHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Import button pressed", pressed)
		}
	}
	
	// Incognito icon button
	Rectangle {
		color: incognitoHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 80
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: incognitoHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Incognito button pressed", pressed)
		}
	}
	
	// Settings icon button
	Rectangle {
		color: settingsHoverHandler.hovered ? palette.base : palette.dark
		radius: 4
		width: 35
		height: 35
		x: parent.width - 40
		y: 3
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: settingsHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("Settings button pressed", pressed)
		}
	}
	
	// BrowseMode icon button
	Rectangle {
		color: browseHoverHandler.hovered ? palette.dark : palette.base
		radius: 4
		width: 35
		height: 35
		x: 1
		y: parent.height - 34
		z: 2
		
		Text {
			text: ""
			font.family: "UbuntuNerdFont"
			color: palette.text
			anchors.centerIn: parent
		}
		
		HoverHandler {
			id: browseHoverHandler
			acceptedDevices: PointerDevice.Mouse
			cursorShape: Qt.PointingHandCursor
		}
		
		TapHandler {
			onPressedChanged: console.log("BrowseMode button pressed", pressed)
		}
	}
	
	// Explaination text viewer
	Rectangle {
		color: palette.base
		radius: 4
		width: 300
		height: 35
		x: 40
		y: parent.height - 34
		z: 2
		
		Text {
			text: "Example text here"
			font.family: "UbuntuNerdFont"
			color: palette.text
			horizontalAlignment: Text.AlignLeft
			verticalAlignment: Text.AlignVCenter
			anchors.left: parent.left
			anchors.verticalCenter: parent.verticalCenter
		}
	}
	
	// Background
	Rectangle {
		color: palette.base
		anchors.fill: parent
		
		Rectangle {
		color: palette.base
		width: parent.width
		height: parent.height - 74
		x: 0
		y: 40
		z: 1
	
		ScrollView {
			width: parent.width
			height: parent.height - 20
			ScrollBar.horizontal.interactive: false
			ScrollBar.vertical.interactive: false
				
			GridLayout {
				id: browseGrid
				columns: 5
				
				Repeater {
					model: 30
					delegate: Button {
						text: "Button" + index
						Layout.preferredWidth: 130
						Layout.preferredHeight: 200
					}
				}
			}
		}
	}
	}
}
