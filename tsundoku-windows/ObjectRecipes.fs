// :
// : Object Recipes
// :
module ObjectRecipes

// Manga Class
type Manga(nameInput: string, chaptersInput: int) =
   member this.name = nameInput
   member this.chapters = chaptersInput
