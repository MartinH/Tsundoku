// :
// :--------- Modules included
// :
open System
open System.IO
open System.Drawing
open System.Drawing.Drawing2D
open System.Drawing.Text
open System.Text
//open System.Math
open System.Windows.Forms
open System.Windows.Input
open System.Collections.Generic
open System.Diagnostics
open ObjectRecipes


// :
// :--------- Global variables
// :
let configFilePath = "tsundoku.conf"
let standardMangaLibraryDirectoryName = "LocalMangaLibrary"
let mutable mangaList: Manga list = []
let mutable mangaIndexToRead: int = 0
let mutable totalOfMangaPagesInChapter: int = 0
let mutable mangaPageToRead: int = 1
let mutable readerMouseIsDown = false
let mutable readerMousePoint = PointF(0.0f, 0.0f)
let mutable viewLayout = false // false = Poster-Grid, true = Block-Grid
let ImageMatrix = new Matrix()
let mutable selectedSettingsCategory: int = 1 // 1 = General, 2 = Reader, 3 = Theming
// Window movement
let mutable initialMousePosition = Point(0, 0)
let mutable isDragging = false
let mutable newWidth = 0
let mutable newHeight = 0
// GUI related
let mutable browseBuffer = []
// Fonts
let fontCollection = new PrivateFontCollection()
fontCollection.AddFontFile("Assets\\Ubuntu_Nerd_Font.ttf")
fontCollection.AddFontFile("Assets\\Noto_Sans_Regular_Nerd.ttf")
let ubuntuTextFont = new Font(fontCollection.Families.[0], 10.0f)
let ubuntuSmallTextFont = new Font(fontCollection.Families.[0], 10.0f)
let ubuntuIconFont = new Font(fontCollection.Families.[0], 12.0f)
let notoTextFont = new Font(fontCollection.Families.[1], 9.0f)
// Configuration settings
let mutable defaultBrowseViewMode = "U" // U = Unset. P = PosterGrid, G = Grid

// :
// :--------- Generic functions
// :

let CheckAndReturnPageType(inputName: string) : string =
   if File.Exists(inputName + "png") then 
      inputName + "png" 
   elif File.Exists(inputName + "jpg") then
      inputName + "jpg"
   elif File.Exists(inputName + "bmp") then
      inputName + "bmp"
   elif File.Exists(inputName + "jpeg") then
      inputName + "jpeg"
   else
      failwith "File not found"

let findAndSetPagesInChapter(inputIndexOfManga: int) : int = // (inputIndexOfManga: int, inputChapterNumber: int)
   printfn "filler"
   0
   // return 0 for success, 1 for fail

// Clean Name String - this replaces _ with normal space. this is needed for presenting manga title
let CleanMangaName(inputName: string) : string =
   let mutable myStringBuilder = new StringBuilder(inputName)
   for i = 0 to inputName.Length - 1 do
      if inputName.[i] = '_' then
         myStringBuilder.Chars(i) <- ' '
   myStringBuilder.ToString()

// Shorten Name String - this shortens the strng to fit into label that presents manga title
let ShortenMangaName(inputName: string) : string =
   let maxAmountOfChars = 21
   let mutable myStringBuilder = new StringBuilder("")
   if inputName.Length > (maxAmountOfChars + 3) then
      for i = 0 to maxAmountOfChars do
         myStringBuilder.Append(inputName.[i])
      myStringBuilder.Append('.')
      myStringBuilder.Append('.')
      myStringBuilder.ToString()
   else inputName

// Parse configuration file
let ParseConfig() =
   //printfn $"Parsing configuration file: {configFilePath}"
   // parsed configuration options
   let mutable parsedLibaryPath = ""
   let mutable parsedLibaryName = ""
   let mutable parsedDefaultBrowseViewMode = ""
   let mutable parsedCleanBrowseMode = ""
   let mutable parsedRememberRotation = ""
   let mutable parsedRememberPosition = ""
   let mutable parsedBackgroundColor = ""
   let mutable parsedPanelsColor = ""
   let mutable parsedTextColor = ""
   let mutable parsedPanelsSelectBackgroundColor = ""
   let mutable parsedPanelsSelectTextColor = ""
   let mutable parsedReaderBackgroundColor = ""
   let mutable parsedFrameColor = ""
   if File.Exists(configFilePath) then
      let mutable parserProgress = 1
      try
         for lineRead in File.ReadLines(configFilePath) do
            if lineRead.StartsWith("#") = false then
               //printfn "%s" lineRead
               match parserProgress with
               | 0 -> printfn "Parser error!"
               | 1 ->
                  if lineRead.Length > 12 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 12 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 2
                     parsedLibaryPath <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedLibaryPath
                  else parserProgress <- 0
               | 2 ->
                  if lineRead.Length > 12 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 12 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 3
                     parsedLibaryName <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedLibaryName
                  else parserProgress <- 0
               | 3 ->
                  if lineRead.Length > 24 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 24 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 4
                     parsedDefaultBrowseViewMode <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedDefaultBrowseViewMode
                  else parserProgress <- 0
               | 4 ->
                  if lineRead.Length > 22 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 22 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 5
                     parsedCleanBrowseMode <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedCleanBrowseMode
                  else parserProgress <- 0
               | 5 ->
                  if lineRead.Length > 17 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 17 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 6
                     parsedRememberRotation <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedRememberRotation
                  else parserProgress <- 0
               | 6 ->
                  if lineRead.Length > 17 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 17 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 7
                     parsedRememberPosition <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedRememberPosition
                  else parserProgress <- 0
               | 7 ->
                  if lineRead.Length > 16 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 16 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 8
                     parsedBackgroundColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedBackgroundColor
                  else parserProgress <- 0
               | 8 ->
                  if lineRead.Length > 12 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 12 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 9
                     parsedPanelsColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedPanelsColor
                  else parserProgress <- 0
               | 9 ->
                  if lineRead.Length > 10 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 10 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 10
                     parsedTextColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedTextColor
                  else parserProgress <- 0
               | 10 ->
                  if lineRead.Length > 30 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 30 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 11
                     parsedPanelsSelectBackgroundColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedPanelsSelectBackgroundColor
                  else parserProgress <- 0
               | 11 ->
                  if lineRead.Length > 24 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 24 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 12
                     parsedPanelsSelectTextColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedPanelsSelectTextColor
                  else parserProgress <- 0
               | 12 ->
                  if lineRead.Length > 23 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 23 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 13
                     parsedReaderBackgroundColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedReaderBackgroundColor
                  else parserProgress <- 0
               | 13 ->
                  if lineRead.Length > 18 then
                     let mutable tempStringBuilder = new StringBuilder("")
                     for i = 0 to lineRead.Length - 1 do
                        if i > 18 then
                           tempStringBuilder.Append(lineRead.[i]) |> ignore
                     parserProgress <- 20
                     parsedFrameColor <- tempStringBuilder.ToString()
                     //printfn "PARSED: %s" parsedFrameColor
                  else parserProgress <- 0
               | _ -> ()
      with
      | _ as ex -> printfn "file error %s" ex.Message
   // Set Configuration Settings variables:
   defaultBrowseViewMode <- parsedDefaultBrowseViewMode

// CorrectedCover
let CorrectedCover(inputString: string) : string =
   if File.Exists($"{inputString}.jpg") = false then
      $"{inputString}.png"
   else
      $"{inputString}.jpg"

// :
// :--------- Manga objects related functions
// :

// Parse and Create Manga objects from library folder
let ParseAndCreateMangaFromLibraryFolder() : int =
   if Directory.Exists(standardMangaLibraryDirectoryName) then
      let mangaDirectories = Directory.GetDirectories(standardMangaLibraryDirectoryName)
      // if sub-directories was found:
      if mangaDirectories.Length > 0 then
         for mangaDir in mangaDirectories do
            let tempDirInfo = DirectoryInfo $"{mangaDir}"
            let chapterDirectories = Directory.GetDirectories(mangaDir)
            // if sub-directories found:
            if chapterDirectories.Length > 0 then
               let createdManga = Manga(tempDirInfo.Name, chapterDirectories.Length)
               mangaList <- createdManga :: mangaList
      0 // return run normally
   else 
      1 // return error

// Fill browseBuffer - First page in ascending order
let FillBrowseBufferFirstAscending() =
   // remove all contents of browseBuffer list
   browseBuffer <- []
   // fill browseBuffer list
   for i = 0 to 29 do
      if mangaList.Length >= i then
         let tempIndexToAdd = i + 1
         browseBuffer <- browseBuffer @ [tempIndexToAdd]
      else
         browseBuffer <- browseBuffer @ [0]

// Fill browseBuffer - First page in descending order
let FillBrowseBufferFirstDecending() =
   // remove all contents of browseBuffer list
   browseBuffer <- []
   // fill browseBuffer list
   if mangaList.Length <> 0 then
      for i =  29 downto  0 do
         printfn "i came to this point. don't know how to solve it futher jet"
   else
      for i = 0 to 29 do
         browseBuffer <- browseBuffer @ [0]

// :
// :--------- Custom User Controls
// :

// : Manga Poster
type MangaPoster(inputTitle: string, inputIndex: int) as this =
   inherit UserControl()
   // Sub controls
   let imageBox = new PictureBox()
   let titleLabel = new Label()
   // image path
   let tempImagePath: string = CorrectedCover($"LocalMangaLibrary\\{mangaList[inputIndex].name}\\cover")
   // Contents
   do
      this.Size <- new Size(175, 200)
      this.Location <- new Point(0, 0)
      this.BackColor <- Color.Black
      // imageBox
      imageBox.Location <- new Point(1, 1)
      imageBox.BackColor <- Color.Black
      imageBox.SizeMode <- PictureBoxSizeMode.Zoom
      imageBox.Image <- Image.FromFile(tempImagePath)
      imageBox.Cursor <- Cursors.Hand
      imageBox.Width <- 173
      imageBox.Height <- 175
      imageBox.Enabled <- true
      imageBox.Visible <- true
      // titleLabel 
      titleLabel.Text <- CleanMangaName($"{inputTitle}")
      titleLabel.Text <- ShortenMangaName($"{titleLabel.Text}")
      titleLabel.Font <- ubuntuTextFont
      titleLabel.TextAlign <- ContentAlignment.MiddleLeft
      titleLabel.Padding <- new Padding(3,0,3,0)
      titleLabel.Location <- new Point(0, 176)
      titleLabel.BackColor <- Color.Black
      titleLabel.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      titleLabel.AutoSize <- false
      titleLabel.Cursor <- Cursors.Hand
      titleLabel.Width <- 175
      titleLabel.Height <- 24
      titleLabel.Enabled <- true
      titleLabel.Visible <- true
      // add controls to control
      this.Controls.Add(imageBox :> Control)
      this.Controls.Add(titleLabel :> Control)
   override this.OnMouseDown(e: MouseEventArgs) =
      base.OnMouseDown(e)
      printfn "click happen"


// : Manga Grid Item
type MangaGridItem(inputTitle: string, inputIndex: int) as this =
   inherit UserControl()
   // Sub controls
   let titleLabel = new Label()
   let readLabelIcon = new Label()
   let continueLabelIcon = new Label()
   let chaptersLabel = new Label()
   // Contents
   do
      this.Size <- new Size(270, 60)
      this.Location <- new Point(0, 0)
      this.BackColor <- Color.Black
      // titleLabel
      titleLabel.Text <- CleanMangaName($"{inputTitle}")
      titleLabel.Font <- ubuntuTextFont
      titleLabel.TextAlign <- ContentAlignment.MiddleLeft
      titleLabel.Location <- new Point(1, 1)
      titleLabel.BackColor <- Color.Black
      titleLabel.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      titleLabel.AutoSize <- false
      titleLabel.Cursor <- Cursors.Hand
      titleLabel.Width <- 268
      titleLabel.Height <- 30
      titleLabel.Enabled <- true
      titleLabel.Visible <- true
      // readLabelIcon
      readLabelIcon.Text <- ""
      readLabelIcon.Font <- ubuntuIconFont
      readLabelIcon.TextAlign <- ContentAlignment.MiddleCenter
      readLabelIcon.Location <- new Point(1, 32)
      readLabelIcon.BackColor <- Color.Black
      readLabelIcon.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      readLabelIcon.AutoSize <- false
      readLabelIcon.Cursor <- Cursors.Hand
      readLabelIcon.Width <- 30
      readLabelIcon.Height <- 27
      readLabelIcon.Enabled <- true
      readLabelIcon.Visible <- true
      // continueLabelIcon
      continueLabelIcon.Text <- ""
      continueLabelIcon.Font <- ubuntuIconFont
      continueLabelIcon.TextAlign <- ContentAlignment.MiddleCenter
      continueLabelIcon.Location <- new Point(40, 32)
      continueLabelIcon.BackColor <- Color.Black
      continueLabelIcon.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      continueLabelIcon.AutoSize <- false
      continueLabelIcon.Cursor <- Cursors.Hand
      continueLabelIcon.Width <- 30
      continueLabelIcon.Height <- 27
      continueLabelIcon.Enabled <- true
      continueLabelIcon.Visible <- true
      // chaptersLabel
      chaptersLabel.Text <- $"{mangaList[inputIndex].chapters.ToString()} "
      chaptersLabel.Font <- ubuntuIconFont
      chaptersLabel.TextAlign <- ContentAlignment.MiddleRight
      chaptersLabel.Location <- new Point(200, 32)
      chaptersLabel.BackColor <- Color.Black
      chaptersLabel.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      chaptersLabel.AutoSize <- false
      chaptersLabel.Width <- 69
      chaptersLabel.Height <- 27
      chaptersLabel.Enabled <- true
      chaptersLabel.Visible <- true
      // add controls to control
      this.Controls.Add(titleLabel :> Control)
      this.Controls.Add(readLabelIcon :> Control)
      this.Controls.Add(continueLabelIcon :> Control)
      this.Controls.Add(chaptersLabel :> Control)


// :
// :--------- Reader form
// :

// Form properties
let readerForm = new Form()
readerForm.Text <- "Reader"
readerForm.FormBorderStyle <- FormBorderStyle.Sizable
readerForm.Width <- 600
readerForm.Height <- 800
readerForm.BackColor <- ColorTranslator.FromHtml("#161616")
readerForm.AutoScroll <- false

// Form Controls
let drawSurfucePictureBox = new PictureBox()
drawSurfucePictureBox.Location <- new Point(1, 1)
drawSurfucePictureBox.BackColor <- Color.Black // ColorTranslator.FromHtml("#161616")
drawSurfucePictureBox.Width <- readerForm.Width - 30
drawSurfucePictureBox.Height <- readerForm.Height - 30
drawSurfucePictureBox.Enabled <- true
drawSurfucePictureBox.Visible <- true
readerForm.Controls.Add(drawSurfucePictureBox)
let mutable readingPage = new Bitmap(10,10)

// ### Form Events

// Paint event
let readerPaintHandler (sender: obj) (e: PaintEventArgs) =
   let g = e.Graphics
   g.Transform <- ImageMatrix
   //g.DrawLine(Pens.Blue, 0, 0, readerForm.Width, readerForm.Height)
   //let testImage = Image.FromFile("LocalMangaLibrary\\Boku_x_Kano\\Chapter_01\\Page_9.png")
   readingPage.SetResolution(g.DpiX, g.DpiY)
   g.DrawImage(readingPage, 10, 10)
   // readingPage.Dispose()

// Onload form event
let readerActivatedHandler (e: EventArgs) =
   printfn "Activated event triggered"
   // set form title
   readerForm.Text <- $"{CleanMangaName(mangaList[mangaIndexToRead].name)} - Page 1"
   // contruct PageNamePath based on all suppored image types
   let tempPageName = CheckAndReturnPageType($"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_1.")
   printfn $"{tempPageName}"
   // set form size
   let tempPage = Image.FromFile(tempPageName)
   let currentScreen = Screen.FromHandle(readerForm.Handle)
   if currentScreen.Bounds.Width > tempPage.Width + 30 then
      readerForm.Width <- tempPage.Width + 38
      drawSurfucePictureBox.Width <- tempPage.Width + 36
   if currentScreen.Bounds.Height > tempPage.Height + 30 then
      readerForm.Height <- tempPage.Height + 61
      drawSurfucePictureBox.Height <- tempPage.Height + 59
   else
      readerForm.Height <- currentScreen.Bounds.Height - 40
   // set readingPage
   readingPage <- Bitmap(tempPageName)
   tempPage.Dispose()
// Handlers and event hooks
drawSurfucePictureBox.Paint.AddHandler(new PaintEventHandler(readerPaintHandler))
readerForm.Activated.Add(readerActivatedHandler)
readerForm.KeyDown.Add(fun args ->
   let tempPoint = new PointF(100.0f, 100.0f)
   if args.Modifiers = Keys.Control then
      match args.KeyCode with
      | Keys.Right -> printfn "Keyboard input: Right arrow" //ImageMatrix.RotateAt(-10, 100, 100)
      | Keys.Left -> printfn "Keyboard input: Left arrow" //ImageMatrix.RotateAt(10, 100, 100)
      | _ -> ()
   else
      match args.KeyCode with
      | Keys.Q -> 
               printfn "Keyboard input: Q"
               ImageMatrix.RotateAt(-10.0f, tempPoint)
               drawSurfucePictureBox.Invalidate()
      | Keys.E -> 
               printfn "Keyboard input: E"
               ImageMatrix.RotateAt(10.0f, tempPoint)
               drawSurfucePictureBox.Invalidate()
      | Keys.R ->
               printfn "keyboard input: R"
               ImageMatrix.Reset()
               drawSurfucePictureBox.Invalidate()
      | Keys.Right ->
               printfn "keyboard input: Right Arrow"
               //mangaPageToRead <- mangaPageToRead + 1
               let tempPagNumber = mangaPageToRead + 1
               printfn $"number: {tempPagNumber}"
               let testPathA = $"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_{tempPagNumber}.png"
               let testPathB = $"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_{tempPagNumber}.jpg"
               //printfn $"Page to read value: {mangaPageToRead}"
               if File.Exists(testPathA) = true then 
                  let tempPageName = CheckAndReturnPageType($"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_{tempPagNumber}.")
                  readingPage <- Bitmap(tempPageName)
                  readerForm.Text <- $"{CleanMangaName(mangaList[mangaIndexToRead].name)} - Page {mangaPageToRead}"
                  mangaPageToRead = mangaPageToRead + 1
                  drawSurfucePictureBox.Invalidate()
               elif File.Exists(testPathB) = true then
                  let tempPageName = CheckAndReturnPageType($"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_{tempPagNumber}.")
                  readingPage <- Bitmap(tempPageName)
                  readerForm.Text <- $"{CleanMangaName(mangaList[mangaIndexToRead].name)} - Page {mangaPageToRead}"
                  mangaPageToRead = mangaPageToRead + 1
                  drawSurfucePictureBox.Invalidate()
               else
                  printfn "page does not exists:"
      | Keys.Left ->
               printfn "keyboard input: Left Arrow"
               mangaPageToRead <- mangaPageToRead - 1
               //printfn $"Page to read value: {mangaPageToRead}"
               let tempPageName = CheckAndReturnPageType($"LocalMangaLibrary\\{mangaList[mangaIndexToRead].name}\\Chapter_01\\Page_{mangaPageToRead}.")
               readingPage <- Bitmap(tempPageName)
               readerForm.Text <- $"{CleanMangaName(mangaList[mangaIndexToRead].name)} - Page {mangaPageToRead}"
               drawSurfucePictureBox.Invalidate()
      | _ -> ()
)
readerForm.Closed.Add(fun _ -> 
   mangaPageToRead = 1
   readingPage.Dispose()
)
drawSurfucePictureBox.MouseWheel.Add(fun args ->
   let delta = args.Delta
   if delta > 0 then
      // Scroll up logic
      printfn "Mouse scroll up"
      ImageMatrix.Scale(1.1f, 1.1f, MatrixOrder.Append)
      drawSurfucePictureBox.Invalidate()
   else
      // Scroll down logic
      printfn "Mouse scroll down"
      ImageMatrix.Scale(0.9f, 0.9f, MatrixOrder.Append)
      drawSurfucePictureBox.Invalidate()
   //args.Handled <- true
)
drawSurfucePictureBox.MouseDown.Add(fun args ->
   printfn "Mouse is down"
   readerMouseIsDown <- true
   readerMousePoint <- args.Location
)
drawSurfucePictureBox.MouseMove.Add(fun args ->
   if readerMouseIsDown = true then
      printfn $"{args.X}"
      ImageMatrix.Translate(float32(args.X) - float32(readerMousePoint.X), float32(args.Y) - float32(readerMousePoint.Y))
      drawSurfucePictureBox.Invalidate()
      |> ignore
)
drawSurfucePictureBox.MouseUp.Add(fun args -> 
   printfn "Mouse is up"
   readerMouseIsDown <- false
)


// :
// :--------- Settings form
// :

// Form properties
let settingsForm = new Form()
settingsForm.Text <- "Settings"
settingsForm.Width <- 700
settingsForm.Height <- 400
settingsForm.BackColor <- Color.Black
settingsForm.AutoScroll <- false
settingsForm.FormBorderStyle <- FormBorderStyle.FixedSingle
settingsForm.MaximizeBox <- false
settingsForm.MinimizeBox <- false

// ### Form GUI Elements

// Content Panel
let settingsContentPanel = new Panel()
settingsContentPanel.BackColor <- ColorTranslator.FromHtml("#161616")
settingsContentPanel.Location <- new Point(200,0)
settingsContentPanel.Width <- 500
settingsContentPanel.Height <- settingsForm.Height
settingsContentPanel.Enabled <- true
settingsContentPanel.Visible <- true
settingsForm.Controls.Add(settingsContentPanel)

// Library folder header label
let libraryFolderHeaderLabel = new Label()
libraryFolderHeaderLabel.Location <- new Point(30,20)
libraryFolderHeaderLabel.BackColor <- ColorTranslator.FromHtml("#161616") // 161616 test color: 829BFF
libraryFolderHeaderLabel.AutoSize = false
libraryFolderHeaderLabel.Width <- 300
libraryFolderHeaderLabel.Height <- 40
libraryFolderHeaderLabel.Font <- ubuntuIconFont
libraryFolderHeaderLabel.ForeColor <- Color.White
libraryFolderHeaderLabel.Text <- "Library path"
libraryFolderHeaderLabel.TextAlign <- ContentAlignment.MiddleLeft
settingsContentPanel.Controls.Add(libraryFolderHeaderLabel)
libraryFolderHeaderLabel.Enabled <- true
libraryFolderHeaderLabel.Visible <- true

// Default window size header label
let windowSizeHeaderLabel = new Label()
windowSizeHeaderLabel.Location <- new Point(30,100)
windowSizeHeaderLabel.BackColor <- ColorTranslator.FromHtml("#161616")
windowSizeHeaderLabel.AutoSize = false
windowSizeHeaderLabel.Width <- 300
windowSizeHeaderLabel.Height <- 40
windowSizeHeaderLabel.Font <- ubuntuIconFont
windowSizeHeaderLabel.ForeColor <- Color.White
windowSizeHeaderLabel.Text <- "Window size"
windowSizeHeaderLabel.TextAlign <- ContentAlignment.MiddleLeft
settingsContentPanel.Controls.Add(windowSizeHeaderLabel)
windowSizeHeaderLabel.Enabled <- true
windowSizeHeaderLabel.Visible <- true

// Library title header label
let libraryNameHeaderLabel = Label()
libraryNameHeaderLabel.Location <- new Point(30,180)
libraryNameHeaderLabel.BackColor <- ColorTranslator.FromHtml("#161616")
libraryNameHeaderLabel.AutoSize = false
libraryNameHeaderLabel.Width <- 300
libraryNameHeaderLabel.Height <- 40
libraryNameHeaderLabel.Font <- ubuntuIconFont
libraryNameHeaderLabel.ForeColor <- Color.White
libraryNameHeaderLabel.Text <- "Library name"
libraryNameHeaderLabel.TextAlign <- ContentAlignment.MiddleLeft
settingsContentPanel.Controls.Add(libraryNameHeaderLabel)
libraryNameHeaderLabel.Enabled <- true
libraryNameHeaderLabel.Visible <- true

// remember Browse View Mode header label
let rememberBrowseViewModeHeaderLabel = new Label()
rememberBrowseViewModeHeaderLabel.Location <- new Point(30,260)
rememberBrowseViewModeHeaderLabel.BackColor <- ColorTranslator.FromHtml("#161616")
rememberBrowseViewModeHeaderLabel.AutoSize = false
rememberBrowseViewModeHeaderLabel.Width <- 300
rememberBrowseViewModeHeaderLabel.Height <- 40
rememberBrowseViewModeHeaderLabel.Font <- ubuntuIconFont
rememberBrowseViewModeHeaderLabel.ForeColor <- Color.White
rememberBrowseViewModeHeaderLabel.Text <- "Remember browse view mode"
rememberBrowseViewModeHeaderLabel.TextAlign <- ContentAlignment.MiddleLeft
settingsContentPanel.Controls.Add(rememberBrowseViewModeHeaderLabel)
rememberBrowseViewModeHeaderLabel.Enabled <- true
rememberBrowseViewModeHeaderLabel.Visible <- true

// General side tab label button
let settingsGeneralTabButton = new Label()
settingsGeneralTabButton.Location <- new Point(0,0)
settingsGeneralTabButton.BackColor <- ColorTranslator.FromHtml("#0F0F0F")
settingsGeneralTabButton.AutoSize = false
settingsGeneralTabButton.Width <- 200
settingsGeneralTabButton.Height <- 60
settingsGeneralTabButton.Cursor <- Cursors.Hand
settingsGeneralTabButton.Font <- ubuntuIconFont
settingsGeneralTabButton.ForeColor <- Color.White
settingsGeneralTabButton.Text <- "General"
settingsGeneralTabButton.TextAlign <- ContentAlignment.MiddleCenter
settingsGeneralTabButton.Enabled <- true
settingsGeneralTabButton.Visible <- true
settingsForm.Controls.Add(settingsGeneralTabButton)
settingsGeneralTabButton.MouseEnter.Add(fun _ -> 
   settingsGeneralTabButton.ForeColor <- Color.White
   settingsGeneralTabButton.BackColor <- ColorTranslator.FromHtml("#0F0F0F")
)
settingsGeneralTabButton.MouseLeave.Add(fun _ -> 
   if selectedSettingsCategory <> 1 then
      settingsGeneralTabButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      settingsGeneralTabButton.BackColor <- Color.Black
)

// Reader side tab label button
let settingsReaderTabButton = new Label()
settingsReaderTabButton.Location <- new Point(0,61)
settingsReaderTabButton.BackColor <- Color.Black
settingsReaderTabButton.AutoSize = false
settingsReaderTabButton.Width <- 200
settingsReaderTabButton.Height <- 60
settingsReaderTabButton.Cursor <- Cursors.Hand
settingsReaderTabButton.Font <- ubuntuIconFont
settingsReaderTabButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
settingsReaderTabButton.Text <- "Reader"
settingsReaderTabButton.TextAlign <- ContentAlignment.MiddleCenter
settingsReaderTabButton.Enabled <- true
settingsReaderTabButton.Visible <- true
settingsForm.Controls.Add(settingsReaderTabButton)
settingsReaderTabButton.MouseEnter.Add(fun _ ->
   settingsReaderTabButton.ForeColor <- Color.White
   settingsReaderTabButton.BackColor <- ColorTranslator.FromHtml("#0F0F0F")
)
settingsReaderTabButton.MouseLeave.Add(fun _ ->
   if selectedSettingsCategory <> 2 then
      settingsReaderTabButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      settingsReaderTabButton.BackColor <- Color.Black
)

// Theming side tab label button
let settingsThemingTabButton = new Label()
settingsThemingTabButton.Location <- new Point(0, 122)
settingsThemingTabButton.BackColor <- Color.Black
settingsThemingTabButton.AutoSize = false
settingsThemingTabButton.Width <- 200
settingsThemingTabButton.Height <- 60
settingsThemingTabButton.Cursor <- Cursors.Hand
settingsThemingTabButton.Font <- ubuntuIconFont
settingsThemingTabButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
settingsThemingTabButton.Text <- "Theming"
settingsThemingTabButton.TextAlign <- ContentAlignment.MiddleCenter
settingsThemingTabButton.Enabled <- true
settingsThemingTabButton.Visible <- true
settingsForm.Controls.Add(settingsThemingTabButton)
settingsThemingTabButton.MouseEnter.Add(fun _ ->
   settingsThemingTabButton.ForeColor <- Color.White
   settingsThemingTabButton.BackColor <- ColorTranslator.FromHtml("#0F0F0F")
)
settingsThemingTabButton.MouseLeave.Add(fun _ ->
   if selectedSettingsCategory <> 3 then
      settingsThemingTabButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
      settingsThemingTabButton.BackColor <- Color.Black
)

// version label
let settingsVersionLabel = new Label()
settingsVersionLabel.Location <- new Point(0, 320)
settingsVersionLabel.BackColor <- Color.Black
settingsVersionLabel.AutoSize = false
settingsVersionLabel.Width <- 200
settingsVersionLabel.Height <- 40
settingsVersionLabel.Font <- ubuntuTextFont
settingsVersionLabel.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
settingsVersionLabel.Text <- "Version 1.0"
settingsVersionLabel.TextAlign <- ContentAlignment.MiddleCenter
settingsVersionLabel.Enabled <- true
settingsVersionLabel.Visible <- true
settingsForm.Controls.Add(settingsVersionLabel)

// :
// :--------- Main form
// :

// predefined GUI elements declearations
let actionInfoLabel = new Label()
let layoutIconButton = new Label()
let resizeIconButton = new Label()
let mangaViewPanel = new Panel()
let mangaViewPanelCover = new PictureBox()
// temp test MangaPoster control - only use for testing
//let testMangaP = new MangaPoster("Test Manga")
//let testMangaG = new MangaGridItem("Boku wa Tomodashi ga Sukaunai", 13)

// Form properties
let applicationForm = new Form()
applicationForm.Text <- "Tsundoku"
applicationForm.FormBorderStyle <- FormBorderStyle.None
applicationForm.Width <- 1000
applicationForm.Height <- 800
applicationForm.KeyPreview <- true
applicationForm.BackColor <- ColorTranslator.FromHtml("#161616")
applicationForm.AutoScroll <- false

// ### Form Events

// Application Form Mouse Move events
let applicationFormMouseDownReciver (sender: obj) (e: MouseEventArgs) =
   if e.Button = MouseButtons.Left then
      initialMousePosition <- e.Location
      isDragging <- true
      (sender :?> Control).Capture <- true
      (sender :?> Control).Cursor <- Cursors.Hand
   //actionInfoLabel.Text <- "Mouse is being clicked"
let applicationFormMouseMoveReciver (sender: obj) (e: MouseEventArgs) =
   if isDragging && e.Button = MouseButtons.Left then
      let currentMousePosition = (sender :?> Control).PointToScreen(e.Location)
      let displacement = Point(currentMousePosition.X - initialMousePosition.X, currentMousePosition.Y - initialMousePosition.Y)
      let newX = (sender :?> Control).Location.X + displacement.X
      let newY = (sender :?> Control).Location.Y + displacement.Y
      applicationForm.Location <- Point(newX, newY)
      // (sender :?> Control).Location <- Point(newX, newY)
   //actionInfoLabel.Text <- "Mouse is being moved"
let applicationFormMouseUpReciver (sender: obj) (e: MouseEventArgs) =
   if e.Button = MouseButtons.Left then
      isDragging <- false
      (sender :?> Control).Capture <- false
      (sender :?> Control).Cursor <- Cursors.Default
   actionInfoLabel.Text <- " "

// mangaNameLabelsClicked
let mangaNameLabelsClicked (sender: Label) =
   printfn "DEBUG start:"
   printfn($"sender name: {sender.Name}")
   let nameLength = sender.Name.Length
   printfn($"lenght found: {nameLength}")
   match nameLength with
   | 3 -> mangaIndexToRead <- Convert.ToInt32(sender.Name.[2].ToString())
   | n when n > 3 -> mangaIndexToRead <- Convert.ToInt32(sender.Name.Substring(3))
   | _ -> ()
   printfn($"manga index to start: {mangaIndexToRead}")

// Action info label MouseEnter-Reciver
let actionInfoEnterReciver (sender: Label) =
   sender.ForeColor <- Color.White
   match sender.Name with
   | "HeaderTitleButton" -> actionInfoLabel.Text <- "Change library name"
   | "SettingsButton" -> actionInfoLabel.Text <- "Open settings"
   | "SearchButton" -> actionInfoLabel.Text <- "Search by text"
   | "ImportButton" -> actionInfoLabel.Text <- "Import manga"
   | "TagSearchButton" -> actionInfoLabel.Text <- "Search by tag and text"
   | "IncognitoButton" -> actionInfoLabel.Text <- "Enter incognito mode"
   | "BookmarksButton" -> actionInfoLabel.Text <- "View bookmarks"
   | "CloseButton" -> actionInfoLabel.Text <- "Close program"
   | "MaximizeButton" -> actionInfoLabel.Text <- "Maximize program (alittle bit broken)"
   | "MinimizeButton" -> actionInfoLabel.Text <- "Minimize program"
   | "LayoutButton" -> actionInfoLabel.Text <- "Change layout"
   | "ResizeButton" -> actionInfoLabel.Text <- "Resize window"
   | _ -> ()

// Action info label MouseLeave-Reciver
let actionInfoLeaveReciver (sender: Label) =
   sender.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
   actionInfoLabel.Text <- " "

// Click on Manga Event - click on cover
let clickOnManga (sender: PictureBox) =
   try
      printfn "DEBUG start:"
      printfn($"sender name: {sender.Name}")
      let nameLength = sender.Name.Length
      printfn($"lenght found: {nameLength}")
      match nameLength with
      | 3 -> mangaIndexToRead <- Convert.ToInt32(sender.Name.[2].ToString())
      | n when n > 3 -> mangaIndexToRead <- Convert.ToInt32(sender.Name.Substring(3))
      | _ -> ()
      printfn($"manga index to start: {mangaIndexToRead}")
      totalOfMangaPagesInChapter <- findAndSetPagesInChapter(mangaIndexToRead) // this returns amount of pages in chapter. for now it gives for chater 1.
      readerForm.ShowDialog(applicationForm)
   finally
      printfn "somthing wrong happen"

// New drawPosterGridLayout
let drawPosterGridLayout() =
   // How many width of (poster-control + spacing) fit to an row
   let amountPerRow = applicationForm.Width / 179
   let mutable sideSpacing = applicationForm.Width - (179 * (amountPerRow - 1))
   sideSpacing <- sideSpacing / 2
   let mutable startX = sideSpacing
   let mutable startY = 2
   let mutable itemRowCounter = 0
   // the loop
   for item in browseBuffer do
      if item <> 0 && item <> -1 then
         itemRowCounter <- itemRowCounter + 1
         let correctedIndex = item - 1
         if mangaList.Length - 1 >= correctedIndex then
            let customControl = new MangaPoster(mangaList[correctedIndex].name, correctedIndex)
            mangaViewPanel.Controls.Add(customControl)
            customControl.Location <- new Point(startX, startY)
            let mutable tempSwitch = false
            if itemRowCounter = amountPerRow - 1 then
               startY <- startY + 205
               tempSwitch <- true
            if tempSwitch = false then
               startX <- startX + 179
            else
               startX <- sideSpacing
               itemRowCounter <- 0
         else
            printfn $"Index skipped: {item}, correctedIndex: {correctedIndex}"

// New drawGridLayout
let drawGridLayout() =
   // How many width of (poster-control + spacing) fit to an row
   let amountPerRow = applicationForm.Width / 274
   let mutable sideSpacing = applicationForm.Width - (274 * (amountPerRow - 1))
   sideSpacing <- sideSpacing / 2
   let mutable startX = sideSpacing
   let mutable startY = 2
   let mutable itemRowCounter = 0
   // the loop
   for item in browseBuffer do
      if item <> 0 && item <> -1 then
         itemRowCounter <- itemRowCounter + 1
         let correctedIndex = item - 1
         if mangaList.Length - 1 >= correctedIndex then
            let customControl = new MangaGridItem(mangaList[correctedIndex].name, correctedIndex)
            mangaViewPanel.Controls.Add(customControl)
            customControl.Location <- new Point(startX, startY)
            let mutable tempSwitch = false
            if itemRowCounter = amountPerRow - 1 then
               startY <- startY + 64
               tempSwitch <- true
            if tempSwitch = false then
               startX <- startX + 274
            else
               startX <- sideSpacing
               itemRowCounter <- 0
         else
            printfn $"Index skipped: {item}, correctedIndex: {correctedIndex}"

// Toggle Layout
let toggleLayout (sender: Label) =
   if viewLayout = false then
      sender.Text <- ""
      viewLayout <- true
   else if viewLayout = true then
      sender.Text <- "󱗼"
      viewLayout <- false

// ### Form GUI Elements

// Form events
//applicationForm.Load.Add(drawPosterGridLayout)

// Test MangaPoster adjusting - Only for testing
//testMangaP.Location <- new Point(200, 500)
//applicationForm.Controls.Add(testMangaP)
//testMangaG.Location <- new Point(400, 500)
//applicationForm.Controls.Add(testMangaG)

// Header panel
let headerPanel = new Panel()
headerPanel.Location <- new Point(0,0)
headerPanel.BackColor <- Color.Black
headerPanel.Width <- applicationForm.Width
headerPanel.Height <- 34
headerPanel.Enabled <- true
headerPanel.Visible <- true
applicationForm.Controls.Add(headerPanel)
headerPanel.MouseDown.Add(applicationFormMouseDownReciver headerPanel)
headerPanel.MouseMove.Add(applicationFormMouseMoveReciver headerPanel)
headerPanel.MouseUp.Add(applicationFormMouseUpReciver headerPanel)

// Header title
let headerTitle = new Label()
headerTitle.Location <- new Point(0,0)
headerTitle.Text <- "Tsundoku"
headerTitle.Name <- "HeaderTitleButton"
headerTitle.Font <- ubuntuSmallTextFont
headerTitle.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
headerTitle.BackColor <- Color.Black
headerTitle.TextAlign <- ContentAlignment.MiddleLeft
headerTitle.Cursor <- Cursors.Hand
headerTitle.AutoSize <- false
headerTitle.Width <- 200
headerTitle.Height <- 33
headerTitle.Padding <- new Padding(4, 0, 0, 0)
headerTitle.Enabled <- true
headerTitle.Visible <- true
headerPanel.Controls.Add(headerTitle)
headerTitle.MouseEnter.Add(fun _ -> actionInfoEnterReciver headerTitle)
headerTitle.MouseLeave.Add(fun _ -> actionInfoLeaveReciver headerTitle)

// Close icon button
let closeButton = new Label()
closeButton.Location <- new Point((applicationForm.Width - 36), 0)
closeButton.Name <- "CloseButton"
closeButton.Font <- ubuntuIconFont
closeButton.Text <- ""
closeButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
closeButton.BackColor <- Color.Black
closeButton.TextAlign <- ContentAlignment.MiddleCenter
closeButton.Cursor <- Cursors.Hand
closeButton.AutoSize <- false
closeButton.Width <- 36
closeButton.Height <- 33
closeButton.Padding <- new Padding(0, 2, 0, 0)
closeButton.Enabled <- true
closeButton.Visible <- true
headerPanel.Controls.Add(closeButton)
closeButton.Click.Add(fun _ -> applicationForm.Close())
closeButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver closeButton)
closeButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver closeButton)

// Maximize icon button
let maximizeButton = new Label()
maximizeButton.Location <- new Point((applicationForm.Width - 72), 0)
maximizeButton.Name <- "MaximizeButton"
maximizeButton.Font <- ubuntuIconFont
maximizeButton.Text <- ""
maximizeButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
maximizeButton.BackColor <- Color.Black
maximizeButton.TextAlign <- ContentAlignment.MiddleCenter
maximizeButton.Cursor <- Cursors.Hand
maximizeButton.AutoSize <- false
maximizeButton.Width <- 36
maximizeButton.Height <- 33
maximizeButton.Padding <- new Padding(0, 2, 0, 0)
maximizeButton.Enabled <- true
maximizeButton.Visible <- true
headerPanel.Controls.Add(maximizeButton)
maximizeButton.Click.Add(fun _ -> applicationForm.WindowState <- FormWindowState.Maximized)
maximizeButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver maximizeButton)
maximizeButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver maximizeButton)

// Minimize icon button
let minimizeButton = new Label()
minimizeButton.Location <- new Point((applicationForm.Width - 108), 0)
minimizeButton.Name <- "MinimizeButton"
minimizeButton.Font <- ubuntuIconFont
minimizeButton.Text <- ""
minimizeButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
minimizeButton.BackColor <- Color.Black
minimizeButton.TextAlign <- ContentAlignment.MiddleCenter
minimizeButton.Cursor <- Cursors.Hand
minimizeButton.AutoSize <- false
minimizeButton.Width <- 36
minimizeButton.Height <- 33
minimizeButton.Padding <- new Padding(0, 2, 0, 0)
minimizeButton.Enabled <- true
minimizeButton.Visible <- true
headerPanel.Controls.Add(minimizeButton)
minimizeButton.Click.Add(fun _ -> applicationForm.WindowState <- FormWindowState.Minimized)
minimizeButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver minimizeButton)
minimizeButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver minimizeButton)

// Text search icon button
let textSearchButton = new Label()
textSearchButton.Location <- new Point((applicationForm.Width - 350), 0)
textSearchButton.Name <- "SearchButton"
textSearchButton.Font <- ubuntuIconFont
textSearchButton.Text <- ""
textSearchButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
textSearchButton.BackColor <- Color.Black
textSearchButton.TextAlign <- ContentAlignment.MiddleCenter
textSearchButton.Cursor <- Cursors.Hand
textSearchButton.AutoSize <- false
textSearchButton.Width <- 36
textSearchButton.Height <- 33
textSearchButton.Padding <- new Padding(0, 2, 0, 0)
textSearchButton.Enabled <- true
textSearchButton.Visible <- true
headerPanel.Controls.Add(textSearchButton)
textSearchButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver textSearchButton)
textSearchButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver textSearchButton)

// Tag search icon button
let tagSearchButton = new Label()
tagSearchButton.Location <- new Point((applicationForm.Width - 314), 0)
tagSearchButton.Name <- "TagSearchButton"
tagSearchButton.Font <- ubuntuIconFont
tagSearchButton.Text <- "󱤇"
tagSearchButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
tagSearchButton.BackColor <- Color.Black
tagSearchButton.TextAlign <- ContentAlignment.MiddleCenter
tagSearchButton.Cursor <- Cursors.Hand
tagSearchButton.AutoSize <- false
tagSearchButton.Width <- 36
tagSearchButton.Height <- 33
tagSearchButton.Padding <- new Padding(0, 2, 0, 0)
tagSearchButton.Enabled <- true
tagSearchButton.Visible <- true
headerPanel.Controls.Add(tagSearchButton)
tagSearchButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver tagSearchButton)
tagSearchButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver tagSearchButton)

// Bookmark icon button
let bookmarkButton = new Label()
bookmarkButton.Location <- new Point((applicationForm.Width - 278), 0)
bookmarkButton.Name <- "BookmarksButton"
bookmarkButton.Font <- ubuntuIconFont
bookmarkButton.Text <- "󰸕"
bookmarkButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
bookmarkButton.BackColor <- Color.Black
bookmarkButton.TextAlign <- ContentAlignment.MiddleCenter
bookmarkButton.Cursor <- Cursors.Hand
bookmarkButton.AutoSize <- false
bookmarkButton.Width <- 36
bookmarkButton.Height <- 33
bookmarkButton.Padding <- new Padding(0, 2, 0, 0)
bookmarkButton.Enabled <- true
bookmarkButton.Visible <- true
headerPanel.Controls.Add(bookmarkButton)
bookmarkButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver bookmarkButton)
bookmarkButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver bookmarkButton)

// Import icon button
let importButton = new Label()
importButton.Location <- new Point((applicationForm.Width - 242), 0)
importButton.Name <- "ImportButton"
importButton.Font <- ubuntuIconFont
importButton.Text <- ""
importButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
importButton.BackColor <- Color.Black
importButton.TextAlign <- ContentAlignment.MiddleCenter
importButton.Cursor <- Cursors.Hand
importButton.AutoSize <- false
importButton.Width <- 36
importButton.Height <- 33
importButton.Padding <- new Padding(0, 2, 0, 0)
importButton.Enabled <- true
importButton.Visible <- true
headerPanel.Controls.Add(importButton)
importButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver importButton)
importButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver importButton)

// Incognito icon button
let incognitoButton = new Label()
incognitoButton.Location <- new Point((applicationForm.Width - 206), 0)
incognitoButton.Name <- "IncognitoButton"
incognitoButton.Font <- ubuntuIconFont
incognitoButton.Text <- "󰗹"
incognitoButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
incognitoButton.BackColor <- Color.Black
incognitoButton.TextAlign <- ContentAlignment.MiddleCenter
incognitoButton.Cursor <- Cursors.Hand
incognitoButton.AutoSize <- false
incognitoButton.Width <- 36
incognitoButton.Height <- 33
incognitoButton.Padding <- new Padding(0, 2, 0, 0)
incognitoButton.Enabled <- true
incognitoButton.Visible <- true
headerPanel.Controls.Add(incognitoButton)
incognitoButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver incognitoButton)
incognitoButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver incognitoButton)

// Settings icon button
let settingsButton = new Label()
settingsButton.Location <- new Point((applicationForm.Width - 170), 0)
settingsButton.Name <- "SettingsButton"
settingsButton.Font <- ubuntuIconFont
settingsButton.Text <- ""
settingsButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
settingsButton.BackColor <- Color.Black
settingsButton.TextAlign <- ContentAlignment.MiddleCenter
settingsButton.Cursor <- Cursors.Hand
settingsButton.AutoSize <- false
settingsButton.Width <- 36
settingsButton.Height <- 33
settingsButton.Padding <- new Padding(0, 2, 0, 0)
settingsButton.Enabled <- true
settingsButton.Visible <- true
headerPanel.Controls.Add(settingsButton)
settingsButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver settingsButton)
settingsButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver settingsButton)
settingsButton.Click.Add(fun _ ->
   settingsForm.ShowDialog(applicationForm)
   |> ignore
)

// Action info label
actionInfoLabel.Location <- new Point(50, (applicationForm.Height - 35))
actionInfoLabel.Font <- notoTextFont
actionInfoLabel.Text <- " "
actionInfoLabel.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
actionInfoLabel.AutoSize <- false
actionInfoLabel.Width <- 300
actionInfoLabel.Height <- 33
actionInfoLabel.TextAlign <- ContentAlignment.MiddleLeft
actionInfoLabel.Enabled <- true
actionInfoLabel.Visible <- true
applicationForm.Controls.Add(actionInfoLabel)

// # Roller icon buttons
// Layout icon button
layoutIconButton.Location <- new Point(3, (applicationForm.Height - 35))
layoutIconButton.Name <- "LayoutButton"
layoutIconButton.Font <- ubuntuIconFont
layoutIconButton.Text <- "󱗼"
layoutIconButton.TextAlign <- ContentAlignment.MiddleCenter
layoutIconButton.Cursor <- Cursors.Hand
layoutIconButton.AutoSize <- false
layoutIconButton.Width <- 36
layoutIconButton.Height <- 33
layoutIconButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
layoutIconButton.BackColor <- ColorTranslator.FromHtml("#161616")
layoutIconButton.Enabled <- true
layoutIconButton.Visible <- true
applicationForm.Controls.Add(layoutIconButton)
layoutIconButton.Click.Add(fun _ -> toggleLayout layoutIconButton)
layoutIconButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver layoutIconButton)
layoutIconButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver layoutIconButton)
// resize form mouse functions
// Application Form Mouse Move events
let applicationFormResizeMouseDownReciver (sender: obj) (e: MouseEventArgs) =
   if e.Button = MouseButtons.Left then
      initialMousePosition <- e.Location
      isDragging <- true
      (sender :?> Control).Capture <- true
      (sender :?> Control).Cursor <- Cursors.SizeNWSE
   //actionInfoLabel.Text <- "Mouse is being clicked"
let applicationFormResizeMouseMoveReciver (sender: obj) (e: MouseEventArgs) =
   if isDragging && e.Button = MouseButtons.Left then
      let currentMousePosition = (sender :?> Control).PointToScreen(e.Location)
      //let displacement = Point(currentMousePosition.X - initialMousePosition.X, currentMousePosition.Y - initialMousePosition.Y)
      newWidth <- applicationForm.Width
      newHeight <- applicationForm.Height
      //applicationForm.Location <- Point(newX, newY)
      // (sender :?> Control).Location <- Point(newX, newY)
   //actionInfoLabel.Text <- "Mouse is being moved"
let applicationFormResizeMouseUpReciver (sender: obj) (e: MouseEventArgs) =
   let currentMousePosition = (sender :?> Control).PointToScreen(e.Location)
   if e.Button = MouseButtons.Left then
      if currentMousePosition.X > initialMousePosition.X then
         newWidth <- newWidth + (currentMousePosition.X - initialMousePosition.X) //(sender :?> Control).Location.X + displacement.X
      elif currentMousePosition.X < initialMousePosition.X then newWidth <- newWidth - (initialMousePosition.X - currentMousePosition.X)
      //else false
      elif currentMousePosition.Y > initialMousePosition.Y then
         newHeight <- newHeight + (currentMousePosition.Y - initialMousePosition.Y) //(sender :?> Control).Location.Y + displacement.Y
      elif currentMousePosition.Y < initialMousePosition.Y then newHeight <- newHeight - (initialMousePosition.Y - currentMousePosition.Y)
      else printfn("huh")
      applicationForm.Width <- newWidth
      printfn $"{newWidth}"
      applicationForm.Height <- newHeight
      printfn $"{newHeight}"
      isDragging <- false
      (sender :?> Control).Capture <- false
      (sender :?> Control).Cursor <- Cursors.Default
   actionInfoLabel.Text <- " "
// mangaNameLabelsClicked
// Resize icon button
resizeIconButton.Location <- new Point((applicationForm.Width - 36), (applicationForm.Height - 33))
resizeIconButton.Name <- "ResizeButton"
resizeIconButton.Font <- ubuntuIconFont
resizeIconButton.Text <- "󰁃"
resizeIconButton.TextAlign <- ContentAlignment.MiddleCenter
resizeIconButton.Cursor <- Cursors.Hand
resizeIconButton.AutoSize <- false
resizeIconButton.Width <- 36
resizeIconButton.Height <- 33
resizeIconButton.ForeColor <- ColorTranslator.FromHtml("#d8d8d8")
resizeIconButton.BackColor <- ColorTranslator.FromHtml("#161616")
resizeIconButton.Enabled <- true
resizeIconButton.Visible <- true
applicationForm.Controls.Add(resizeIconButton)
resizeIconButton.MouseDown.Add(applicationFormResizeMouseDownReciver resizeIconButton)
resizeIconButton.MouseMove.Add(applicationFormResizeMouseMoveReciver resizeIconButton)
resizeIconButton.MouseUp.Add(applicationFormResizeMouseUpReciver resizeIconButton)
resizeIconButton.MouseEnter.Add(fun _ -> actionInfoEnterReciver resizeIconButton)
resizeIconButton.MouseLeave.Add(fun _ -> actionInfoLeaveReciver resizeIconButton)
// # Manga View Panel
// the panel itself
mangaViewPanel.Location <- new Point(1, 38)
mangaViewPanel.BackColor <- ColorTranslator.FromHtml("#161616")
mangaViewPanel.Width <- (applicationForm.Width - 2)
mangaViewPanel.Height <- (applicationForm.Height - 82)
mangaViewPanel.AutoScroll <- true
mangaViewPanel.Enabled <- true
mangaViewPanel.Visible <- true
applicationForm.Controls.Add(mangaViewPanel)
// panel cover
mangaViewPanelCover.Location <- new Point((mangaViewPanel.Width - 15), 38)
mangaViewPanelCover.BackColor <- ColorTranslator.FromHtml("#161616")
mangaViewPanelCover.Width <- 22
mangaViewPanelCover.Height <- mangaViewPanel.Height
mangaViewPanelCover.Enabled <- true
mangaViewPanelCover.Visible <- true
applicationForm.Controls.Add(mangaViewPanelCover)
mangaViewPanelCover.BringToFront()

// Apply configuration
let ApplyConfig() =
   printfn "About to apply configuration"
   printfn $"brose mode: {defaultBrowseViewMode}"
   FillBrowseBufferFirstAscending()


// :
// :--------- Main / Entry point
// :
[<EntryPoint>]
let main argv = 
   ParseConfig()
   //ApplyConfig()
   if ParseAndCreateMangaFromLibraryFolder() = 0 then
      printfn "Parsing PASS"
   else printfn "Parsing FAIL"
   // fill browseBuffer
   if defaultBrowseViewMode = "P" then
      printfn "FILLING browseBuffer starts:"
      printfn " "
      FillBrowseBufferFirstAscending()
      printfn "Drawing Poster-grid"
      drawPosterGridLayout()
      printfn " "
   elif defaultBrowseViewMode = "G" then
      printfn "FILLING browseBuffer starts:"
      printfn " "
      FillBrowseBufferFirstAscending()
      printfn "Drawing Grid"
      drawGridLayout()
      printfn " "
   // run main appliction form
   Application.Run(applicationForm)
   0
